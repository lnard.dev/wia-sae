FROM node:16.15.1

WORKDIR /opt/wia-sae

COPY . .

RUN npm install --quiet

EXPOSE 3000

CMD [ "node","/opt/wia-saw/index.js" ]