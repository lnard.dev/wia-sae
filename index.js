import express from 'express'
import axios from 'axios'

const app = express()

app.use(express.json())

app.get('/api/wia',async (req,res)=>{
  const {limit, page} = req.query
  const datos = {
    "proyectID": "colmena-arl-11000",
    "datasetId": "ColmenaARL",
    "tableId" : "colmena_arl_mxn",
    "limit" : limit,
    "offset" : (page-1)* limit
  }
  const resp = await axios.post('https://wia-web-api-gw-5j8fyx1b.uc.gateway.dev/conectionCXM?key=AIzaSyClC9KoixrqyM3CcO24a29OI3u4e3Vzv4c',datos)
  const {data} = resp
  
  res.status(200).json({message:'Data was updated successfully'})
})

app.listen(3000, () => console.log('Server on port 3000'))