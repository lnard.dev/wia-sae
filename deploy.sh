#!/bin/bash

echo 'COMMAND ------------------------ service docker start'
service docker start

echo 'COMMAND ------------------------ cd /opt/wia-saw'
cd /opt/wia-saw

echo 'COMMAND ------------------------ cd docker-compose down'
docker-compose down

echo 'COMMAND ------------------------ docker rmi -f node_v1'
docker rmi -f node_v1

echo 'COMMAND ------------------------ git config --global http.sslVerify false'
git config --global http.sslVerify false

echo 'COMMAND ------------------------ git checkout master'
git checkout master

echo 'COMMAND ------------------------ git pull origin master'
git pull origin master

echo 'COMMAND ------------------------ docker build -t node_v1 .'
docker build -t node_v1 .

echo 'COMMAND ------------------------ docker-compose up -d'
docker-compose up -d --scale web=2
